//. (25 pts) En Bash escribir funciones que dada una cadena de caracteres:
//➔ Devuelva solamente las letras consonantes. Por ejemplo, si recibe ’algoritmos’ o ’logaritmos’
//debe devolver ’lgrtms’.
//➔ Devuelva solamente las letras vocales. Por ejemplo, si recibe ’sin consonantes’ debe devolver ’i
//ooae’.
//➔ Reemplace cada vocal por su siguiente vocal. Por ejemplo, si recibe ’vestuario’ debe devolver
//’vistaerou’.
//➔ Indique si se trata de un palíndromo. Por ejemplo, ’anita lava la tina’ es un palíndromo (se lee
//igual de izquierda a derecha que de derecha a izquierda)

#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>



char vocales[] = "aeiou";
char aux;
int x; 
int vocal(char caracteres)
{
 for (x=0; vocales[x]; x++ )
 {

    if (caracteres == vocales[x])
    {   

    return 1;
    }
 
    return 0;

 }

}

int consonante (char caracteres)
{

return isalpha(caracteres) && !vocal(caracteres);

}


int main ()
{

char palabra[] = "logaritmo";
int i;
for (i=0; palabra[i]; i++)
{
 char letraA = palabra[i];

 if (vocal(letraA))
 {
    printf("\n vocal");
    printf(" '%c' ", letraA);

 }
 else(consonante(letraA)); 
 
  printf("\n consonates" );
  printf(" '%c' ", letraA);
 
 
}

}
