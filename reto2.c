//2. (25 pts) Implementar en C el algoritmo de Euclides para calcular el máximo común divisor de dos
//números n y m, dado por los siguientes pasos.
//1. Teniendo n y m, se obtiene r, el resto de la división entera de m/n.
//2. Si r es cero, n es el m.c.d de los valores iniciales.
//3. Se reemplaza m ← n, n ← r, y se vuelve al primer paso.
//Hacer un seguimiento del algoritmo implementado para los siguientes pares de números: (15,9);
//(9,15); (10,8); (12,6). 


#include <stdlib.h>
#include <stdio.h>


int mcd (int m, int n)
{
int aux=0;
while (n != 0)
{

    aux=n;
    n = m%n;
    m=aux;

}

}


int main (void)
{

    printf ("para 15,9; %d\n", mcd (15,9));
    printf ("para 9,15; %d\n", mcd (9,15));
    printf ("para 10,8; %d\n", mcd (10,8));
    printf ("para 12,6; %d\n", mcd (12,6));
    

}